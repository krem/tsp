__PROJEKT O TSP__
=================

Najpierw trzeba sie zabrac za wspolna strukture do wczytywania danych.
Zaklepie przetwarzanie wejscia na strukture o postaci:
```
vector< vector< int > > graf;
```
gdzie `graf[a][b]` bedzie oznaczac dlugosc krawedzi miedzy wierzcholkami `a` i `b`.


Zrealizujmy rozwiazania jako metody postaci:
```
vector<int> metoda_rozwiazania(const vector< vector< int > > &graf);
```
albo
```
#include"common.h"
vector<int> metoda_rozwiazania(const GRAPH_VECTOR &graf);
```
gdzie zwracamy sciezke. Jej dlugosc wynikowa wyliczy osobny fragment kodu od
przedstawiania wynikow, na zewnatrz algorytmow rozwiazujacych TSP.

Kod podzielimy na pliki - 1 plik na 1 metode rozwiazania, plus jeden na metody wczytujace graf
i prezentujace wyniki, i jeden plik main.
