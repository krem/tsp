CC=g++
CFLAGS=-c -std=c++0x
LDFLAGS=
SOURCES=main.cpp utils.cpp bruteForce.cpp bruteForceExtended.cpp \
		closestFirst.cpp the2OPT.cpp mst.cpp christofidesAlgorithm.cpp
SRCS=$(SOURCES)
SRCS+=utils.h bruteForce.h bruteForceExtended.h closestFirst.h \
		the2OPT.h mst.h christofidesAlgorithm.h
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=main

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o: depend
	$(CC) $(CFLAGS) $< -o $@

depend: .depend

.depend: $(SRCS)
	rm -f ./.depend
	$(CC) $(CFLAGS) -MM $^ > ./.depend;

include .depend

stdafx.h:
	touch stdafx.h

clean:
	rm *.o .depend

.PHONY: clean
