// Piotr Markowski

#include "bruteForceExtended.h"

using namespace std;

int findShortestEdge(const GRAPH_VECTOR &graph)
{
	int shortestEdge = -1;
	for(int i = 0; i < graph.size(); i++)
	{
		for(int j = 0; j < (graph[i]).size(); j++)
		{
			if(graph[i][j] < shortestEdge || shortestEdge == -1)
			{
				shortestEdge = graph[i][j];
			}
		}
	}
	return shortestEdge;
}

vector<int> bruteForceExtended(const GRAPH_VECTOR &graph, int currentVertex, int tempSum, int &sum, vector<int> &tempPath, vector<bool> &visited, int &upperBound, int shortestEdge)
{
	vector<int> path;
	tempPath.push_back(currentVertex);
	if (tempPath.size() == graph.size())
	{
		//Because it's a complete graph, we can assume that the path
		//can be made into a cycle with exactly one edge
		
		tempSum += graph[currentVertex][0];
		if (tempSum < sum || sum == -1)
		{
			sum = tempSum;
			upperBound = sum;
			path = tempPath;
			path.push_back(0);
		}
		tempSum -= graph[currentVertex][0];
		tempPath.pop_back();
	}
	else
	{
		visited[currentVertex] = true;
		int i;
		for (i = 0; i < graph[currentVertex].size(); i++)
		{
			if (visited[i]) {
				continue;
			}
			tempSum += graph[currentVertex][i];
			int lowerBound = tempSum + (graph.size() - tempPath.size()) * shortestEdge;
			if (lowerBound < upperBound)
			{
				vector<int> newPath = bruteForceExtended(graph, i, tempSum, sum, tempPath, visited, upperBound, shortestEdge);
				if(newPath.size() != 0) {
					path = newPath;
				}
			}
			tempSum -= graph[currentVertex][i];
		}
		visited[currentVertex] = false;
		tempPath.pop_back();
	}
	return path;
}

vector<int> bruteForceExtended(const GRAPH_VECTOR &graph)
{
	vector<int> tempPath;
	vector<bool> visited (graph.size(), false);
	int shortestEdge = findShortestEdge(graph);
	int upperBound = 2000000000;
	int sum = -1;
	return bruteForceExtended(graph, 0, 0, sum, tempPath, visited, upperBound, shortestEdge);
}
