//Marcin Szymura
#pragma once
#include "common.h"
#include "christofidesAlgorithm.h"
#include "utils.h"

void dfs(int start, std::vector<bool> &visited, ADJACENCY_LIST &graph);
bool isGraphCoherent(ADJACENCY_LIST &a);
int totalLength(ADJACENCY_LIST &a);
bool shouldProduceProperMST(GRAPH_VECTOR &graph, int expectedResult);
bool shouldSelectAllVerticiesWithOddDegree(ADJACENCY_LIST& graph, std::set<int>& solution);
bool shouldComputeProperGraphInducedByVertexSet(std::set<int> &vertexSet, GRAPH_VECTOR &graph);
bool shouldComputeProperHamiltonCycle(std::vector<int> &result, int n);
