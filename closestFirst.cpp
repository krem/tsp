//Piotr Markowski

#include"closestFirst.h"

using namespace std;

vector<int> closestFirst(const GRAPH_VECTOR &graph)
{
	vector<bool> visited(graph.size(), false);
	int currentVertex = 0;
	int nextVertex;
	int shortestPath;
	vector<int> path;
	path.push_back(currentVertex);
	while(path.size() != graph.size())
	{
		shortestPath = -1;
		visited[currentVertex] = true;
		for(int i = 0; i < graph[currentVertex].size(); i++)
		{
			if(visited[i])
			{
				continue;
			}
			if(shortestPath == -1 || graph[currentVertex][i] < shortestPath)
			{
				shortestPath = graph[currentVertex][i];
				nextVertex = i;
			}
		}
		if(shortestPath == -1)
		{
			vector<int> errorVector;
			return errorVector;
		}
		else
		{
			currentVertex = nextVertex;
			path.push_back(currentVertex);
		}
		
	}
	path.push_back(0);
	return path;
}
