//Marcin Szymura
#include "christofidesAlgorithm.h"
#include <cstdio>

using namespace std;

typedef std::vector< std::vector< std::pair<int, int > > > ADJACENCY_LIST;

/*
 * Explanation: sometimes it's more convenient to use ADJACENCY_LIST than ADJACENCY MATRIX.
 * Simply because at some stages of processing solution, we have to work with graphs that are not cliques.
 */
ADJACENCY_LIST calculateMinimumSpanningTree(const GRAPH_VECTOR &graph);
std::set<int> selectVerticesWithOddDegree(ADJACENCY_LIST &spanningTree);
ADJACENCY_LIST* createSubgraphInducedByVertexSet(const GRAPH_VECTOR &graph, std::set<int> &vertexSet);
ADJACENCY_LIST composePerfectMatchingInClique(ADJACENCY_LIST &clique);
ADJACENCY_LIST uniteGraphsToFormMultigraph(ADJACENCY_LIST &complemented, ADJACENCY_LIST &complementing);
std::vector<int> constructEulerCycleInGraph(ADJACENCY_LIST &graph);
std::vector<int> reduceRepeatingVerticesByMakingShortcuts(std::vector<int> &eulerCycle, int graphSize);

int findFirstNotIsolatedVertex(ADJACENCY_LIST &graph);
void eulerCycleDFS(int startNode, std::vector< std::vector<int> > &graph, std::vector<int> &path);

class DisjointSetForest { 
	private:
		vector<int> rep;
		vector<int> rank;
	public:
		DisjointSetForest(int n) {
			rep.resize(n);
			rank.assign(n, 0);
			for(int i = 0; i < n; i++) rep[i] = i;
		}
		
		void Union(int x, int y) {
			x = Find(x); y = Find(y);
			if(rank[x] < rank[y]) rep[x] = y;
			else rep[y] = x;
			if(rank[x] == rank[y]) rank[x]++;
		}
		
		int Find(int x) { return (x != rep[x]) ? rep[x] = Find(rep[x]) : x; }
};

struct Edge {
	int start;
	int end;
	int weight;
	Edge(int s, int e, int w) : start(s), end(e), weight(w) {}
};

bool operator< (const Edge &a, const Edge &b) { return a.weight < b.weight; }

vector<int> christofidesAlgorithm(const GRAPH_VECTOR &metricGraph) {

	ADJACENCY_LIST spanningTree = calculateMinimumSpanningTree(metricGraph);
	set<int> verticesWithOddDegree = selectVerticesWithOddDegree(spanningTree);
	
	ADJACENCY_LIST* inducedSubgraph = createSubgraphInducedByVertexSet(metricGraph, verticesWithOddDegree);
	ADJACENCY_LIST perfectMatchingOfMinimumCost = composePerfectMatchingInClique(*inducedSubgraph);
	
	delete inducedSubgraph;

	ADJACENCY_LIST multigraph = uniteGraphsToFormMultigraph(spanningTree, perfectMatchingOfMinimumCost);
	vector<int> eulerCycle = constructEulerCycleInGraph(multigraph);

	vector<int> reducedEulerCycle = reduceRepeatingVerticesByMakingShortcuts(eulerCycle, metricGraph.size());

	reducedEulerCycle.push_back(reducedEulerCycle[0]);

	return reducedEulerCycle;
}

// Note: implementation based on Kruskal's algorithm
ADJACENCY_LIST calculateMinimumSpanningTree(const GRAPH_VECTOR &graph) {
	DisjointSetForest forest(graph.size());
	vector<Edge> edges;
	
	ADJACENCY_LIST spanningTree(graph.size());
	
	for(int i = 0; i < graph.size(); i++) {
		for(int j = i+1; j < graph[i].size(); j++)
			edges.push_back( Edge(i, j, graph[i][j]) );
	}
	
	sort(edges.begin(), edges.end());
	
	for(int i = 0; i < edges.size(); i++) {
		Edge e = edges[i];
		if(forest.Find(e.start) != forest.Find(e.end)) {
			forest.Union(e.start, e.end);
			spanningTree[e.start].push_back(make_pair(e.end, e.weight));
			spanningTree[e.end].push_back(make_pair(e.start, e.weight));
		}
	}
	
	return spanningTree;
}

set<int> selectVerticesWithOddDegree(ADJACENCY_LIST &spanningTree) {
	set<int> verticies;
	for(int i = 0; i < spanningTree.size(); i++)
		if(spanningTree[i].size() % 2 == 1)
			verticies.insert(i);

	return verticies;
}

ADJACENCY_LIST* createSubgraphInducedByVertexSet(const GRAPH_VECTOR &graph, set<int>& vertexSet) {
	ADJACENCY_LIST *subgraph = new ADJACENCY_LIST(graph.size());
	
	for(set<int>::iterator it = vertexSet.begin(); it != vertexSet.end(); it++)
		for(set<int>::iterator iter = vertexSet.begin(); iter != vertexSet.end(); iter++)
			if( it != iter ) {
				int x = *it;
				int y = *iter;
				(*subgraph)[x].push_back(make_pair(y, graph[x][y]));
				(*subgraph)[y].push_back(make_pair(x, graph[y][x]));
			}
		
	return subgraph;
}

// Note: Naive, greedy implementation working in O(n^2).
ADJACENCY_LIST composePerfectMatchingInClique(ADJACENCY_LIST &clique) {
	ADJACENCY_LIST perfectMatching(clique.size());
	vector<bool> connected(clique.size());
	vector<Edge> edges;
	for(int i = 0; i < clique.size(); i++) {
		for(int j = 0; j < clique[i].size(); j++) {
			edges.push_back(Edge(i, clique[i][j].first, clique[i][j].second));
		}
	}
	sort(edges.begin(), edges.end());
	for(int i = 0; i < edges.size(); i++) {
		if(connected[edges[i].start] == false && connected[edges[i].end] == false) {
			perfectMatching[edges[i].start].push_back(make_pair(edges[i].end, edges[i].weight));
			perfectMatching[edges[i].end].push_back(make_pair(edges[i].start, edges[i].weight));
			connected[edges[i].start] = true;
			connected[edges[i].end] = true;
		}
	}
	return perfectMatching;
}

ADJACENCY_LIST uniteGraphsToFormMultigraph(ADJACENCY_LIST &complemented, ADJACENCY_LIST &complementing) {
	for(int i = 0; i < complementing.size(); i++) {
		for(int j = 0; j < complementing[i].size(); j++) {
			complemented[i].push_back(complementing[i][j]);
		}
	}
	return complemented;
}

vector<int> constructEulerCycleInGraph(ADJACENCY_LIST &graph) {
	vector< vector<int> > matrix(graph.size());
	for(int i = 0; i < graph.size(); i++) {
		matrix[i].resize(graph.size());
		for(int j = 0; j < graph[i].size(); j++) {
			matrix[i][graph[i][j].first]++;
		}
	}
	
	vector<int> path;
	eulerCycleDFS(findFirstNotIsolatedVertex(graph), matrix, path);
	return path;
}

int findFirstNotIsolatedVertex(ADJACENCY_LIST &graph) {
	for(int i = 0; i < graph.size(); i++) if(graph[i].size() > 0) return i;
	return 0;
}

void eulerCycleDFS(int startNode, vector< vector<int> > &graph, vector<int> &path) {
	for(int i = 0; i < graph.size(); i++) {
		while(graph[startNode][i] > 0) {
			graph[startNode][i]--;
			graph[i][startNode]--;
			eulerCycleDFS(i, graph, path);
		}
	}
	path.push_back(startNode);
}

vector<int> reduceRepeatingVerticesByMakingShortcuts(vector<int> &eulerCycle, int graphSize) {
	vector<bool> visited(graphSize, false);
	vector<int> simpleCircle;
	
	for(int i = 0; i < eulerCycle.size(); i++) {
		if(visited[eulerCycle[i]] == false) {
			visited[eulerCycle[i]] = true;
			simpleCircle.push_back(eulerCycle[i]);
		}
	}
	
	return simpleCircle;
}
