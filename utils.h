//Bartosz Prusak

#pragma once

#include"common.h"
#include<cstdio>

//returns adjacency matrix of a graph from file
GRAPH_VECTOR readGraphFromFile(FILE* file);

//prints solution passed as argument 
void writeSolutionToFile(
	FILE* file,
	const std::vector<int> &solution,
	const GRAPH_VECTOR &graph
);

