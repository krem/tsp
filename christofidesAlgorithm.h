//Marcin Szymura
#pragma once

#include "common.h"
#include <algorithm>
#include <utility>
#include <set>

std::vector<int> christofidesAlgorithm(const GRAPH_VECTOR &metricGraph);
