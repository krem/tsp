//Bartosz Prusak
#include<cstdio>
#include<vector>
#include<algorithm>
#include<string>
#include<chrono>
#include<map>
#include<sstream>

#include"common.h"
#include"utils.h"
#include"bruteForce.h"
#include"bruteForceExtended.h"
#include"closestFirst.h"
#include"the2OPT.h"
#include"mst.h"
#include"christofidesAlgorithm.h"

using namespace std;
using namespace std::chrono;

typedef vector<int> (*tsp_solution)(const GRAPH_VECTOR &);

pair< string, tsp_solution > function_roster[] = {
	pair<string, tsp_solution> ("bruteforce", bruteForce),
	pair<string, tsp_solution> ("bruteforce with branch&bound", bruteForceExtended),
	pair<string, tsp_solution> ("closest first", closestFirst),
	pair<string, tsp_solution> ("2-OPT", the2OPT),
	pair<string, tsp_solution> ("2-approximation via MST", mst),
	pair<string, tsp_solution> ("pseudo-1,5-approximation (christofides Algorithm)", christofidesAlgorithm),
};

map< string, int > name_to_roster_id;

const int SOLUTIONS_TO_TEST = 6;

char default_title[] = ""
"===============================\n"
"Default test of all tsp methods\n"
"===============================\n";

void executeTest(pair<string, tsp_solution> method, string fileName) {
	FILE* input = fopen(fileName.c_str(),"r");
	if(!input) {
		fprintf(stderr, "File \"%s\" not found\n", fileName.c_str());
		return;
	}
	
	GRAPH_VECTOR graph = readGraphFromFile(input);
	fclose(input);
//	printf("%s", default_title);
	printf("---------------------------\n");
	high_resolution_clock::time_point before = high_resolution_clock::now();
	vector<int> solution = method.second(graph);
	high_resolution_clock::time_point after = high_resolution_clock::now();
	printf("%s :\n", method.first.c_str());
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>( after - before ).count();
	printf("Testfile: %s\n", fileName.c_str());
	printf("time taken : %lld microseconds\n", (long long)(duration));
	writeSolutionToFile(stdout, solution, graph);
}

int main(int argc, char** argv)
{
	name_to_roster_id["brute"] = 0;
	name_to_roster_id["brutebnb"] = 1;
	name_to_roster_id["closestfirst"] = 2;
	name_to_roster_id["2opt"] = 3;
	name_to_roster_id["2approx"] = 4;
	name_to_roster_id["15approx"] = 5;
	vector<string> testFiles;
	int solutionMethod = -1;
	int repetitions = 1;

	for(int i = 1; i < argc; i++) {
		if(argv[i] == string("-m")) {
			i++;
			if(name_to_roster_id.find(argv[i]) == name_to_roster_id.end()) {
				fprintf(stderr, "solution method \"%s\" not found, skipping\n", argv[i]);
			} else {
				solutionMethod = name_to_roster_id[argv[i]];
			}
		} else if(argv[i] == string("-c")) {
			i++;
			istringstream str(argv[i]);
			str >> repetitions;
		} else {
			testFiles.push_back(argv[i]);
		}
	}
	if(testFiles.empty()) {
		testFiles.push_back("example1.in");
		if(solutionMethod == -1 && repetitions == 1) {
			printf("%s", default_title);
		}
	}

	if(solutionMethod == -1)
		for(int i = 0; i < SOLUTIONS_TO_TEST; i++) {
			for(auto filename : testFiles) {
				for(int _ = 0; _ < repetitions; _++)
					executeTest(function_roster[i], filename);
			}
		}
	else
		for(auto filename : testFiles) {
			for(int _ = 0; _ < repetitions; _++)
				executeTest(function_roster[solutionMethod], filename);
		}

	return 0;
}
