//Bartosz Prusak
#include"utils.h"

using namespace std;

GRAPH_VECTOR readGraphFromFile(FILE* file) {
	int n, m; fscanf(file, "%d%d", &n, &m);
	GRAPH_VECTOR graph(n);
	for(int i = 0; i < n; i++) {
		graph[i].resize(n);
		graph[i][i] = 0;
	}
	for(int i = 0; i < m; i++) {
		int a, b, w; fscanf(file, "%d%d%d", &a, &b, &w);
		a--; b--;
		graph[a][b] = w;
		graph[b][a] = w;
	}
	return graph;
}

void writeSolutionToFile(
	FILE* file,
	const vector<int> &solution,
	const GRAPH_VECTOR &graph
) {
	int sum = 0;
	if(solution.size() <= 0)
	{
		printf("NO SOLUTION FOUND\n");
		return;
	}
	for(int i = 0; i < solution.size()-1; i++)
		sum += graph[solution[i]][solution[i+1]];
	fprintf(file, "Distance: %d\n", sum);
	fprintf(file, "Vertex order: ");
	for(int i = 0; i < solution.size(); i++)
		fprintf(file, "%d ", solution[i]+1);
	fprintf(file, "\n");
}
