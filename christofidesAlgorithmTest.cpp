//Marcin Szymura
#include "christofidesAlgorithmTest.h"

void dfs(int start, std::vector<bool> &visited, ADJACENCY_LIST &graph) {
	visited[start] = true;
	for(int i = 0; i < graph[start].size(); i++) {
		if(!visited[graph[start][i].first]) dfs(graph[start][i].first, visited, graph);
	}
}

bool isGraphCoherent(ADJACENCY_LIST &a) {
	std::vector<bool> visited(a.size(), false);
	dfs(0, visited, a);
	for(int i = 0; i < a.size(); i++) if(!visited[i]) return false;
	return true;
}

int totalLength(ADJACENCY_LIST &a) {
	int sum = 0;
	for(int i = 0; i < a.size(); i++) 
		for(int j = 0; j < a[i].size(); j++)
			sum += a[i][j].second;
	return sum/2;
}

bool shouldProduceProperMST(GRAPH_VECTOR &graph, int expectedResult) {
	ADJACENCY_LIST result = calculateMinimumSpanningTree(graph);
	printf("Calculation performed successfully\n");
	return isGraphCoherent(result) && (totalLength(result) == expectedResult);
}

bool shouldSelectAllVerticiesWithOddDegree(ADJACENCY_LIST& graph, std::set<int>& solution) {
	return solution == selectVerticiesWithOddDegree(graph);
}

bool shouldComputeProperGraphInducedByVertexSet(std::set<int> &vertexSet, GRAPH_VECTOR &graph) {
	ADJACENCY_LIST *subgraph = createSubgraphInducedByVertexSet(graph, vertexSet);
	for(std::set<int>::iterator it = vertexSet.begin(); it != vertexSet.end(); it++) {
		if((*subgraph)[*it].size() != vertexSet.size()-1) return false;
	}
	return true;
}

bool shouldComputeProperHamiltonCycle(std::vector<int> &result, int n) {
	std::vector<bool> visited(n);
	if(result.size() != n) return false;
	for(int i = 0; i < result.size(); i++) {
		if(visited[result[i]] == true) return false;
		visited[result[i]] = true;
	}
	return true;
}
