//Bartosz Prusak
#include "the2OPT.h"
#include<string>
#include<cstdio>
#include<cstdlib>
#include<ctime>
using namespace std;

enum {
	OPERATIONS_PER_LAUNCH = 10000000
};

//I need graph's hash to determine its unique filename for
//pre-processed results
const char BASE64_TAB[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789._";

string numToBase64(unsigned int num) {
	int bit_count = 0;
	char sum = 0;
	string res("");
	for(int i = 0; i < sizeof(num)*8; i++) {
		sum = (sum<<1)+(((1<<i)&num)?1:0);
		bit_count++;
		if(bit_count >= 6) {
			res += BASE64_TAB[sum];
			sum = bit_count = 0;
		}
	}
	return res;
}

enum {
	HASH_P = 1299709,
	HASH_Q = 2147483647
};

unsigned int hashFunc(const GRAPH_VECTOR &graph) {
	unsigned long long res = 0;
	for(int i = 0; i < graph.size(); i++)
		for(int j = 0; j < graph[i].size(); j++) {
			res = ((res*HASH_P)%HASH_Q + graph[i][j] + 23)%HASH_Q;
		}
	return res;
}

vector<int> the2OPT(const GRAPH_VECTOR &graph) {
	//get result filename based on graph's hashcode
	string graph_name = numToBase64(hashFunc(graph)) + ".precalc";

	//attempt to load a precalculated cycle
	vector<int> pre_result(graph.size());
	for(int i = 0; i < graph.size(); i++) {
		pre_result[i] = i;
	}

	FILE* input = fopen(graph_name.c_str(),"r");
	if(input) {
		int n = 0; fscanf(input, "%d", &n);
		if(n == graph.size())
			for(int i = 0; i < n; i++) {
				int x; fscanf(input, "%d", &x);
				pre_result[i] = x;
			}
		fclose(input);
	}

	//transform a cycle into data useful for us
	int *prev = new int[graph.size()], *next = new int[graph.size()];
	long long res = 0;

	for(int i = 0; i < graph.size(); i++) {
		prev[pre_result[(i+1)%graph.size()]] = pre_result[i];
		next[pre_result[(i+1)%graph.size()]] = pre_result[(i+2)%graph.size()];
		res += graph[pre_result[i]][pre_result[(i+1)%graph.size()]];
	}

	//#########################
	//the algorithm itself
	srand(time(0));
	int graph_size = graph.size();

	for(int _ = 0; _ < OPERATIONS_PER_LAUNCH; _++) {
		int a = rand()%graph_size, b = rand()%graph_size;
		if(a == b) {
			b = (b+1+(rand()%(graph_size-1)))%graph_size;
		}
		int a_p = next[a], b_p = prev[b];
		if(a_p == b) continue;
		int dist_before = 0, dist_after = 0;

		dist_before = graph[a][a_p] + graph[b_p][b];
		dist_after = graph[a][b_p] + graph[a_p][b];

		if(dist_after < dist_before) {
			//commented line outputs the improvement that is about to be introduced
			//fprintf(stderr, "%d->%d; %d<-%d : %d < %d \n", a, a_p, b, b_p, dist_after, dist_before);
			int cur = next[a];
			while(cur != b) {
				int c = next[cur];
				next[cur] = prev[cur];
				prev[cur] = c;
				cur = prev[cur];
			}
			next[next[a]] = b;
			prev[prev[b]] = a;
			int temp = next[a];
			next[a] = prev[b];
			prev[b] = temp;
			res -= dist_before - dist_after;
		}
	}

	//#########################

	//transform our data into solution output format
	int cur = 0;
	vector<int> final_res(graph.size()+1);
	for(int i = 0; i < graph.size()+1; i++) {
		final_res[i] = cur;
		cur = next[cur];
	}

	delete[] next;
	delete[] prev;

	//and try to write it to the .precalc file if possible
	FILE* output = fopen(graph_name.c_str(),"w");
	if(output) {
		fprintf(output, "%d\n", (int)graph.size());
		for(int i = 0; i < graph.size(); i++)
			fprintf(output, "%d ", final_res[i]);
		fclose(output);
	}


	return final_res;
}
