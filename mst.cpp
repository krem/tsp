//Bartosz Prusak
#include "mst.h"
#include <set>
#include <cstdio>
#include <algorithm>

using namespace std;

typedef pair<int, int> PI;
typedef pair<int, PI> EDGE;

int find(int x, vector<int>& tr) {
	return (x == tr[x])?x:(tr[x] = find(tr[x],tr));
}

void doUnion(int a, int b, vector<int>& tr) {
	tr[find(a,tr)] = find(b,tr);
}

//result[a][i] == (b, x) -- there is an edge between a and b and is of length x
vector< vector< PI > > make_mst(const GRAPH_VECTOR& graph) {
	int graph_size = graph.size();
	vector< vector< PI > > result(graph_size);
	//find & union
	vector<int> utree(graph_size);
	for(int i = 0; i < graph_size; i++)
		utree[i] = i;
	
	vector<EDGE> edges;
	for(int i = 0; i < graph_size; i++)
		for(int j = i+1; j < graph_size; j++)
			edges.push_back(EDGE(graph[i][j],PI(i,j)));
	sort(edges.begin(), edges.end());

	for(int i = 0; i < edges.size(); i++) {
		int a = edges[i].second.first, b = edges[i].second.second;
		if(find(a,utree) != find(b,utree)) {
			result[a].push_back(PI(b,edges[i].first));
			result[b].push_back(PI(a,edges[i].first));
			doUnion(a,b,utree);
		}
	}

	return result;
}

void dfs(const vector< vector<PI> > &tree, vector<int> &res, int x, int prev = -1) {
    res.push_back(x);
    for (int i = 0; i < tree[x].size(); i++) {
        if (tree[x][i].first == prev) continue;
        dfs(tree, res, tree[x][i].first, x);
    }
}

vector<int> mst(const GRAPH_VECTOR &graph) {
    vector<int> result;
    vector< vector < PI > > tree = make_mst(graph);
    dfs(tree, result, 0);
    result.push_back(0);
    return result;
}
