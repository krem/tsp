//Piotr Markowski

#include "bruteForce.h"

using namespace std;

vector<int> bruteForce(const GRAPH_VECTOR &graph, int currentVertex, int tempSum, int &sum, vector<int> &tempPath, vector<bool> &visited)
{
	vector<int> path;
	tempPath.push_back(currentVertex);
	if (tempPath.size() == graph.size())
	{
		//Because it's a complete graph, we can assume that the path
		//can be made into a cycle with exactly one edge
		tempSum += graph[currentVertex][0];
		if (tempSum < sum || sum == -1)
		{
			sum = tempSum;
			path = tempPath;
			path.push_back(0);
		}
		tempSum -= graph[currentVertex][0];
		tempPath.pop_back();
	}
	else
	{
		visited[currentVertex] = true;
		int i;
		for (i = 0; i < graph[currentVertex].size(); i++)
		{
			if (visited[i])
				continue;
			tempSum += graph[currentVertex][i];
			vector<int> newPath = bruteForce(graph, i, tempSum, sum, tempPath, visited);
			if(newPath.size() != 0)
				path = newPath;
			tempSum -= graph[currentVertex][i];
		}
		visited[currentVertex] = false;
		tempPath.pop_back();
	}
	return path;
}

vector<int> bruteForce(const GRAPH_VECTOR &graph)
{
	vector<int> tempPath;
	vector<bool> visited (graph.size(), false);
	int sum = -1;
	return bruteForce(graph, 0, 0, sum, tempPath, visited);
}
