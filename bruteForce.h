//Bartosz Prusak
#pragma once
#include "common.h"
#include <algorithm>

std::vector<int> bruteForce(
	const GRAPH_VECTOR &graph
);
