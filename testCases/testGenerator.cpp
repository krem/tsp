//Piotr Markowski

#include <fstream>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <vector>
#include <iostream>
#include <cmath>
#include <set>
#include "testGenerator.h"

using namespace std;

void generateGraphFile(string name, int numberOfVerticies, int maxEdge, int favoredPercent, int favorGrade){
	srand(time(NULL));
	ofstream file;
	string path = "examples/" + name;
	file.open(path.c_str());
	file << numberOfVerticies << " ";
	int numberOfEdges = numberOfVerticies*(numberOfVerticies-1)/2;
	file << numberOfEdges << endl;
	int favoredStep;
	if (favoredPercent != 0){
		favoredStep = 100 / favoredPercent;
	}
	int edgeLength;
	for(int i = 1; i < numberOfVerticies; i++){
		for(int j = i+1; j <= numberOfVerticies; j++){
			edgeLength = rand() % maxEdge;
			if ((favoredPercent != 0) && ((((i-1)*numberOfVerticies + j) % favoredStep) == 0))
				edgeLength /= favorGrade;
			file << i << " " << j << " " << edgeLength + 1 << endl;
		}
	}

}


void generateTaxiGraphFile(string name, int numberOfVerticies, int areaEdge){
	if(numberOfVerticies >= areaEdge*areaEdge)
		cout << "Too many verticies for the area!";
	else{
		srand(time(NULL));
		vector< pair< int, int > > verticies;
		set< pair< int, int > > vertSet;
		int x = rand() % areaEdge, y = rand() % areaEdge;
		pair< int, int > vert(x, y);
		for(int i = 0; i < numberOfVerticies; i++){
			while(vertSet.find(vert) != vertSet.end()){
				vert.first = rand() % areaEdge;
				vert.second = rand() % areaEdge;
			}
			verticies.push_back(vert);
			vertSet.insert(vert);
		}
		ofstream file;
		string path = "examples/" + name;
		file.open(path.c_str());
		file << numberOfVerticies << " ";
		int numberOfEdges = numberOfVerticies*(numberOfVerticies-1)/2;
		file << numberOfEdges << endl;
		int edge;
		for(int i = 0; i < numberOfVerticies - 1; i++){
			for(int j = i+1; j < numberOfVerticies; j++){
				int xDistance = verticies[i].first - verticies[j].first;
				if (xDistance < 0)
					xDistance *= -1;
				int yDistance = verticies[i].second - verticies[j].second;
				if (yDistance < 0)
					yDistance *= -1;
				edge = xDistance + yDistance;
				file << i+1 << " " << j+1 << " " << edge << endl;
			}
		}
	}
}
