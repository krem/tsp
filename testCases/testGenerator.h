//Piotr Markowski

#pragma once

#include<fstream>
#include<time.h>
#include<cstdlib>

void generateGraphFile(std::string name, int numberOfVerticies, int maxEdge, int favoredPercent, int favorGrade);

void generateTaxiGraphFile(std::string name, int numberOfVerticies, int areaEdge);
