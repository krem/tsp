//Piotr Markowski
#include <cstdio>
#include "testGenerator.h"
#include <cstring>
#include <sstream>

using namespace std;

int main(){
	ostringstream s1, s2;
	string name;
	int i;
	int j;
	for(i = 10; i <= 1280; i*=2){
		for(j = 1600; j <= 12800; j*=2){
			s1.str("");
			s2.str("");
			s1 << i;
			s2 << j;
			name = "favored_" + s1.str() + "_" + s2.str() + "_1_1000.in";
			generateGraphFile(name, i, j, 1, 1000);
		}
	}
	for(i = 10; i <= 1280; i*=2){
		for(j = 1600; j <= 12800; j*=2){
			s1.str("");
			s2.str("");
			s1 << i;
			s2 << j;
			name = "favored_" + s1.str() + "_" + s2.str() + "_10_1000.in";
			generateGraphFile(name, i, j, 10, 1000);
		}
	}
	for(i = 10; i <= 1280; i*=2){
		for(j = 1600; j <= 12800; j*=2){
			s1.str("");
			s2.str("");
			s1 << i;
			s2 << j;
			name = "favored_" + s1.str() + "_" + s2.str() + "_25_1000.in";
			generateGraphFile(name, i, j, 25, 1000);
		}
	}
}
